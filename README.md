# Struts2-Hibernate开发学生信息管理系统

#### 简介：

本项目通过学生信息管理功能的开发，来介绍Struts2和Hibernate的整合。主要内容包括：Struts2和Hibernate整合，用户登录模块和学生信息管理模块的设计和实现

#### 开发环境

1. JDK1.8
2. mysql80
3. tomact8.5.34
4. maven3.5.4

#### 使用工具

1. IDEA
2. Naicat
3. chrome

### 学到什么

1、掌握Struts2和Hibernate的整合开发

2、能够使用Struts2+Hibernate独立开发信息管理类的项目，进行数据的增删改查。

#### 个人博客

https://blog.csdn.net/LemonSnm

### 登录界面

![登录界面](https://images.gitee.com/uploads/images/2019/0729/132053_1236b246_4912247.png "屏幕截图.png")

### 管理界面
![管理页面](https://images.gitee.com/uploads/images/2019/0729/132258_79d93eb0_4912247.png "屏幕截图.png")
