package com.lemon.db;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;


/**
 * 工具类：获取session
 * @author lemonsun
 */
//单例模式
public class MyHibernateSessionFactory {
    private static SessionFactory sessionFactory;//会话工厂属性
    //构造方法私有化，保证单列模式
    private MyHibernateSessionFactory(){
    }
    //共有静态方法，获取会话工厂对象
    public static SessionFactory getSessionFactory(){
        if(sessionFactory == null){
            //创建配置对象
            Configuration config = new Configuration().configure("hibernate.cfg.xml");
            //创建会话工厂对象
            sessionFactory = config.buildSessionFactory();
            return sessionFactory;
        }else{
            return sessionFactory;
        }
    }


    
}
