package com.lemon.service;

import com.lemon.pojo.Students;

import java.util.List;

/**
 * 学生德尔业务逻辑接口
 * @author lemonsun
 */
public interface StudentDao {

    //查询所有学生资料
    public List<Students> queryAllStudents();

    //根据学生编号查询学生编号
    public Students queryStudentBySid(String sid);

    //添加学生资料
    public boolean addStudent(Students s);

    //修改学生资料
    public boolean updateStudent(Students s);

    //删除学生资料
    public boolean deleteStudent(String sid);
    
}
