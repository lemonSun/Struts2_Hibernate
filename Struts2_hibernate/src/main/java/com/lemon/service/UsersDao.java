package com.lemon.service;

import com.lemon.pojo.Users;

/**
 * 用户业务逻辑接口
 * @author lemonsun
 */
public interface UsersDao {

    //用户登录方法
    public boolean userLogin(Users u);
}
