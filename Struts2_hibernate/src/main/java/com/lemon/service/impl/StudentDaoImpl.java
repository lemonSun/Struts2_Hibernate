package com.lemon.service.impl;

import com.lemon.db.MyHibernateSessionFactory;
import com.lemon.pojo.Students;
import com.lemon.service.StudentDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.io.Serializable;
import java.util.List;

/**
 * 学生业务逻辑接口的实现类
 * @author lemonsun
 */
public class StudentDaoImpl implements StudentDao {

    //查询所有学生资料
    @Override
    public List<Students> queryAllStudents() {

        //事务
        Transaction tx = null;
        //接受查询结果
        List<Students> list = null;
        //hql语句
        String hql = "";
        
        try{
            //获取回话对象session
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            hql = "from Students";
            tx = session.beginTransaction();
            Query query = session.createQuery(hql,Students.class);
            list = query.list();

            tx.commit();
            return list;
        }catch (Exception e){
            e.printStackTrace();
            tx.commit();
            return list;
        }finally {
            if (tx != null){
                tx = null;
            }
        }
    }

    //根据sid查询单个学生详细信息
    @Override
    public Students queryStudentBySid(String sid) {
        //事务
        Transaction tx = null;
        //接收查询结果
        Students s = null;

        try{
            //获取回话对象session
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            s = (Students)session.get(Students.class,sid);

            tx.commit();
            return s;
        }catch (Exception e){
            e.printStackTrace();
            tx.commit();
            return s;
        }finally {
            if (tx != null){
                tx = null;
            }
        }
    }

    //添加学生
    @Override
    public boolean addStudent(Students s) {
        s.setSid(getNewSid()); //设置学生学号
        //事务
        Transaction tx = null;
        //hql语句
        try{
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            session.save(s);
            tx.commit();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            tx.commit();
            return false;
        }finally {
            if (tx != null){
                tx = null;
            }
        }
    }

    @Override
    public boolean updateStudent(Students s) {
        Transaction tx = null;
        try{
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            session.update(s);
            tx.commit();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            tx.commit();
            return false;
        }finally {
            if(tx != null){
                tx = null;
            }
        }
    }

    //删除学生信息：
    @Override
    public boolean deleteStudent(String sid) {

        
        Transaction tx = null;
        try {
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            Students s = session.get(Students.class,sid);
            session.delete(s);
            tx.commit();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            tx.commit();
            return false;
        }finally {
            if(tx != null){
                tx = null;
            }
        }
    }

    //生成学生学号  (改为private，只能这个类使用)
    private String getNewSid(){

        Transaction tx = null;
        String hql = "";
        String sid = null;
        try{
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            tx = session.beginTransaction();
            //获取当前学生最大编号
            hql = "select max(sid) from Students";
            Query query = session.createQuery(hql);
            sid = (String)query.uniqueResult();
            tx.commit();
            if(sid == null || "".equals(sid)){
                //给一个默认的最大编号
                sid ="S0000001";
            }else{
                String temp = sid.substring(1); //取后七位
                int i = Integer.parseInt(temp); //转换为数字
                i++;
                //再还原成字符串
                temp = String.valueOf(i);
                //获取长度
                int len = temp.length();
                //凑够7位
                for (int j = 0; j < 7 - len; j++) {
                    temp = "0" + temp;
                }
                sid = "S" + temp;
            }
            return sid;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }finally {
            if(tx != null){
                tx = null;
            }
        }
    }
}
