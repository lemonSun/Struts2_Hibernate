package com.lemon.service.impl;

import com.lemon.db.MyHibernateSessionFactory;
import com.lemon.pojo.Students;
import com.lemon.pojo.Users;
import com.lemon.service.UsersDao;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

/**
 * @author lemonsun
 */
public class UsersDaoImpl implements UsersDao {

    
    @Override
    public boolean userLogin(Users u) {
        //事务对象
        Transaction tx = null;
        String hql = "";

        try{
            //获取session对象
            Session session = MyHibernateSessionFactory.getSessionFactory().getCurrentSession();
            //开启事务
            tx = session.beginTransaction();
            // ? 两个参数
            hql = "from Users where username = ? and password = ?";
            Query <Users> query = session.createQuery(hql, Users.class);
            //传递参数
            query.setParameter(0,u.getUsername());
            query.setParameter(1,u.getPassword());
            List list = query.list();
            tx.commit(); //提交事务
            //判断是否查到对象
            if(list.size() > 0){
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }finally {
            if(tx != null){
                tx = null;
                
            }
        }
    }
}
