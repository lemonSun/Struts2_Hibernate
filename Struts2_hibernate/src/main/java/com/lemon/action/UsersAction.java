package com.lemon.action;

import com.lemon.pojo.Users;
import com.lemon.service.UsersDao;
import com.lemon.service.impl.UsersDaoImpl;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.interceptor.validation.SkipValidation;

/**
 * 模型驱动方式：
 * @author lemonsun
 */                                                               /*接受表单数据*/
public class UsersAction extends SuperAction implements ModelDriven<Users> {

    private static final long serialVersionUID = -2108191154567430372L;
    private Users user = new Users();

    //用户登录动作
    public String login(){
        UsersDao usersDao = new UsersDaoImpl();
        if(usersDao.userLogin(user)){
            //在session中保存登录成功的用户名
            session.setAttribute("loginUserName",user.getUsername());
            return "login_success";
        }else{
            return "login_failure";
        }
    }
    
    //用户注销方法
    @SkipValidation    /*执行该方法，不进行表单验证*/
    public String logout(){
        if( session.getAttribute("loginUserName") != null) {
            //清除登录用户
            session.removeAttribute("loginUserName");
        }
        return "logout_success";
    }

    //对这里的所有方法进行验证 重写父类方法（ctrl + o）
    //这里只对表单进行验证
    @Override
    public void validate() {
        super.validate();
        //用户名不能为空                        
        if("".equals(user.getUsername().trim())){
            this.addFieldError("usernameError","用户名不能为空！");
        }
        if(user.getPassword().length() < 6){
            this.addFieldError("passwordError","密码长度不少于6位！");
        }
    }



    @Override
    public Users getModel() {
        return this.user;
    }
}
                                                       