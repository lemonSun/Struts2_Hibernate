package com.lemon.action;

import com.lemon.db.MyHibernateSessionFactory;
import com.lemon.pojo.Students;
import com.lemon.pojo.Users;
import com.lemon.service.StudentDao;
import com.lemon.service.impl.StudentDaoImpl;
import com.opensymphony.xwork2.ModelDriven;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

/**
 * 学生Action类
 * @author lemonsun
 */
public class StudentsAction extends SuperAction implements ModelDriven<Students> {
    private static final long serialVersionUID = -2156631264717908166L;
    private Students students = new Students();

    //查询所有学生的动作：
    public String query(){
        StudentDao sdao = new StudentDaoImpl();
        List <Students> list = sdao.queryAllStudents();
        //将集合放进sessio中
             //如果找到了学生记录
        if (list != null && list.size() > 0) {
            session.setAttribute("students_list",list);
            return "query_success";
        }else{
            return "query_success";
        }
    }

    //删除学生动作
    public String delete(){
        StudentDao sdao = new StudentDaoImpl();
        String sid = request.getParameter("sid");
        sdao.deleteStudent(sid); //调用删除方法
        return "delete_success";
    }

    //添加学生
    public String add(){
        StudentDao sdao = new StudentDaoImpl();
        sdao.addStudent(students);
        return "add_success";
    }

    //修改学生资料动作
    public String modify(){
        //获得页面编号
        String sid = request.getParameter("sid");
        StudentDao sdao = new StudentDaoImpl();
        //找到该学生详细信息
        Students students = sdao.queryStudentBySid(sid);
        //保存在session中
        session.setAttribute("modify_students",students);
        return "modify_success";
    }

    //保存修改后的学生资料动作
    public String save(){
        StudentDao sdao = new StudentDaoImpl();
        sdao.updateStudent(students);
        return "save_success";
    }


    @Override
    public Students getModel() {
        return this.students;
    }
}
