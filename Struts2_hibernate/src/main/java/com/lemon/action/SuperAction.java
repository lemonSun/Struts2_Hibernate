package com.lemon.action;


import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.util.ServletContextAware;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 所有Action的父类
 * @author lemonsun
 */                                 /*扩展的过滤器 拦截器功能*/  /*三个接口 方便实现内置对象*/
public class SuperAction extends ActionSupport implements ServletRequestAware, ServletResponseAware, ServletContextAware {


    private static final long serialVersionUID = -2151976000484500367L;
    protected HttpServletRequest request; //请求对象
    protected HttpServletResponse response; //响应对象
    protected HttpSession session; //回话对象
    protected ServletContext application; //全局对象
    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.request =request;
        this.session = this.request.getSession();

    }

    @Override
    public void setServletResponse(HttpServletResponse response) {
        this.response = response;

    }

    @Override
    public void setServletContext(ServletContext application) {
        this.application = application;

    }
}
