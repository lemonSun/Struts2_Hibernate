package com.lemon.impl;

import com.lemon.pojo.Students;
import com.lemon.service.StudentDao;
import com.lemon.service.impl.StudentDaoImpl;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Date;
import java.util.List;

/**
 * @author lemonsun
 */
public class TestStudentDaoImpl {

    //测试查询所有学生
    @Test
    public void testQueryAllStudents(){

        StudentDao studentDao = new StudentDaoImpl();

        List <Students> list = studentDao.queryAllStudents();
        for (Students students : list) {
            System.out.println(students);
        }
    }

/*    //测试生成学生学号
    @Test
    public void testGetNewSid(){
        StudentDaoImpl sdao = new StudentDaoImpl();
        System.out.println(sdao.getNewSid());

    }*/
    @Test
    public void testAddStudents(){
        Students s = new Students();
        s.setSname("李白");
        s.setGender("男");
        s.setBirthday(new Date(new java.util.Date().getTime()));
        s.setAddress("长安");
        StudentDao sdao = new StudentDaoImpl();
        Assert.assertEquals(true, sdao.addStudent(s));
    }

    //测试学生更新
    @Test
    public void testUpdateSyudent(){
        StudentDao sdao = new StudentDaoImpl();
        Students s = new Students();
        s.setSid("S0000008");
        s.setSname("大乔");
        Assert.assertEquals(true,sdao.updateStudent(s));
    }

}
