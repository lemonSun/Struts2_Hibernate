package com.lemon.impl;

import com.lemon.pojo.Users;
import com.lemon.service.UsersDao;
import com.lemon.service.impl.UsersDaoImpl;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author lemonsun
 */
public class TestUsersDaoImpl {

    //测试用户登录
    @Test
    public void testUserLogin(){
        
        Users u = new Users(1,"zhangsan","123456");
        UsersDao udao = new UsersDaoImpl();
        boolean b = udao.userLogin(u);
        Assert.assertEquals(true,b);
        
    }

}
