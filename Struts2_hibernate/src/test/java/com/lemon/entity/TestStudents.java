package com.lemon.entity;

import com.lemon.db.MyHibernateSessionFactory;
import com.lemon.pojo.Students;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.Test;

import java.sql.Date;


/**
 * @author lemonsun
 */
public class TestStudents {

    //查找
    public void findById(){
        SessionFactory sessionFactory = MyHibernateSessionFactory.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        Query<Students> query = session.createQuery("from Students where id = ?", Students.class);
        query.setParameter(0,"S0000001");
        Students students = query.uniqueResult();
        System.out.println(students.getSname() + "\t" + students.getGender());
        session.close();
    }

    //添加测试数据
    @Test
    public void testSaveStudents(){
        SessionFactory sessionFactory = MyHibernateSessionFactory.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        Students s1 = new Students("S0000005" ,"张三","18",new Date(new java.util.Date().getTime()),"中国");
        Students s2 = new Students("S0000006" ,"李四","19",new Date(new java.util.Date().getTime()),"中国");
        Students s3 = new Students("S0000007","王五","20",new Date(new java.util.Date().getTime()),"中国");

        session.save(s1);
        session.save(s2);
        session.save(s3);
        transaction.commit();
        sessionFactory.close();
    }
}
